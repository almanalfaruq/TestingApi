# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180407064954) do

  create_table "pasiens", force: :cascade do |t|
    t.string "id_ktp"
    t.string "nama"
    t.string "alamat"
    t.string "jenis_kelamin"
    t.string "tempat_lahir"
    t.date "tanggal_lahir"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id_ktp"], name: "index_pasiens_on_id_ktp", unique: true
    t.index ["user_id"], name: "index_pasiens_on_user_id"
  end

  create_table "tanda_vitals", force: :cascade do |t|
    t.string "tekanan_darah"
    t.string "nadi"
    t.string "frekuensi_pernapasan"
    t.float "suhu_tubuh"
    t.integer "tinggi_badan"
    t.integer "berat_badan"
    t.string "golongan_darah"
    t.boolean "resus"
    t.integer "pasien_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pasien_id"], name: "index_tanda_vitals_on_pasien_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
