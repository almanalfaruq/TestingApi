class CreateTandaVitals < ActiveRecord::Migration[5.1]
  def change
    create_table :tanda_vitals do |t|
      t.string :tekanan_darah
      t.string :nadi
      t.string :frekuensi_pernapasan
      t.float :suhu_tubuh
      t.integer :tinggi_badan
      t.integer :berat_badan
      t.string :golongan_darah
      t.boolean :resus
      t.references :pasien, foreign_key: true

      t.timestamps
    end
  end
end
