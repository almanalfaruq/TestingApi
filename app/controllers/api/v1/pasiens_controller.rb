module Api
  module V1
	class PasiensController < ApplicationController
	  def index
		pasien = Pasien.all
		render json: { pasien: pasien }, status: :ok
	  end

	  def show
		pasien = Pasien.find(params[:id])
		render json: { pasien: pasien }, status: :ok
	  end

	  def create
		pasien = Pasien.new(pasien_params)
		if pasien.save
		  render json: { pasien: pasien }, status: :ok
		else
		  render json: { pasien: pasien.error }, status: :unprocessable_entity
		end
	  end

	  private

	  def pasien_params
		params.require(:pasien).permit(:id_ktp, :nama, :alamat, :jenis_kelamin, :tempat_lahir, :tanggal_lahir, :user_id)
	  end
	end
  end
end

